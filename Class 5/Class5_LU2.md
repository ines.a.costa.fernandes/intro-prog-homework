# Exercise 1


## Unordered

- Item 1
- Item 2
 - Item 2a
 - Item 2b
  
## Ordered

1. Item 1
2. Item 2
3. Item 3
 1. Item 3a
  1. item 3b
  2. Item 3b

### Trick
1. Item 1
1. Item 2
1. Item 3