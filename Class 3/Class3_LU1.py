# This script is used to solve the class exercises
#Exercise #1
"""
Scope is the the zones in the text editor that a variable has been assigned.
Local scope is a the scope related to a variable that can only be used in a
specific zone and not in all the code editor.
Local scope is a the scope related to a variable that can be whatever in the
text editor, even inside functions.
"""

#Exercise #2
"""
first_var = global
who = local, function hello_world
second_var = global
who_again = local, function hello_other_world
"""

#Exercise #3
"""
Yes. because the print statement uses the variable first_var before the 
variable has been initialized and assigned a value. This is the cause.
"""

#Exercise #4

var1 = 5

def multiply_by_2():
    return var1 * 2

var2 = multiply_by_2()
print(var1, var2)

#Exercise #5
"""
Yes, because the variable _power_ cannot be printed, it is a local variable of
the function generate_power.
I expect print(a) to print 2 ** 10 = 1024 because power is a globar variable
"""

#Exercise #6
"""
I expect power to be the same (10) because the variable power defined inside
the  function is local, doesn't affect the global variable power, outside the
function.
"""

#Exercise #7
"""
I expect to print oranges and bananas and afterwards to print coins and 
chocolates
I don't see the same thing in both prints because in the first print the function
uses the local variables _what and _for and in the second print the global
variables are used.
"""

#Exercise #8
# Same as before

# Exercise #9
def my_name(first_name = 'Wilson', last_name = 'Ramos'):
    print(first_name, last_name)
    return

my_name()
my_name(last_name = 'Snow', first_name = 'Ines')
my_name('Ines', 'Snow')

# Exercise #10
"""
Prints Sam Hopkins
Prints Ricardo Pereira
Prints Jose Maria
"""

# Exercise #11
def print_message(name = '', message = 'Good Morning'):
    print(message, name)
    return

print_message()
print_message(message = 'You know nothing', name = 'John Snow')
    




